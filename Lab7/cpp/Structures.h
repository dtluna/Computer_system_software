#include <string>
#include <iostream>
#include "defines.h"
using namespace std;

struct record{
	string pathToFile;
	unsigned long long int startingOffSet;
	unsigned long long int blocksCount;
	bool isUsed;
	record(){
		isUsed = false;
		pathToFile = "";
		startingOffSet = 0;
		blocksCount = 0;
	}
};

struct masterFileTable
{
	record Records[recordsNumber];
	unsigned long long int fullRecordsCount;
	unsigned long long int ReferenseToLastFreeBlock;
	masterFileTable(){
		fullRecordsCount = 0;
		ReferenseToLastFreeBlock = sizeof(masterFileTable);
	}
};

struct block{
	char inform[blockSize];
	unsigned long long int referenceToNextBlock;
	block(){
		for (int i = 0; i < blockSize; i++){
			inform[i] = '\0';
		}
		referenceToNextBlock = -1;
	}
};
