//#include <Windows.h>
#include <iostream>
//#include <conio.h>
#include <cstdlib>
#include <fstream>
#include "Structures.h"
#include <string.h>
#include "getch.h"
using namespace std;

void menu(){
	cout << "1. Format Disk" << endl << "2. Add new file" << endl << "3. Delete File" << endl <<"4. Show File"<<endl<< "5. Show All" << endl<<"6. Renew File"<<endl<<"7. Exit"<<endl;
}

class FS{
public:
	masterFileTable mft;
	fstream file;
	
	FS(){
		file.open(disk, ios::binary | ios :: in | ios :: out);
		file.read((char*)&mft, sizeof(masterFileTable));
		int i = 0;
	}
	void formatDisk(){
		char *nullSymbol;
		block bl;
		file.close();
		file.open(disk, ios::binary | ios::in | ios::out);
		file.write((char*)new masterFileTable(), sizeof(masterFileTable));
		for (int i = 0; i < (endingOffset - sizeof(masterFileTable))/24; i++){
			file.write((char*)&bl, sizeof(block));
		}
		file.close();
		file.open(disk, ios::binary | ios::in | ios::out);
		file.read((char*)&mft, sizeof(masterFileTable));
	}
	
	int fileWasFound(string pathToFile){
		for (int i = 0; i < recordsNumber; i++){
			if (pathToFile == mft.Records[i].pathToFile){
				return i;
			}
		}
		return -1;
	}

	void updateMFT(record fileRecord){
		for (int i = 0; i < recordsNumber; i++){
			if (mft.Records[i].isUsed == false){
				mft.Records[i] = fileRecord;
				file.close();
				file.open(disk, ios::binary | ios::in | ios::out);
				file.write((char*)&mft, sizeof(masterFileTable));
				break;
			}
		}
	}

	void updateMFT(){
		file.close();
		file.open(disk, ios::binary | ios::in | ios::out);
		file.write((char*)&mft, sizeof(masterFileTable));
	}

	void leftShift(int ref){
		int ref1;
		int j, k;
		record temp;
		for (int i = 0; i < recordsNumber; i++){
			if (mft.Records[i].isUsed == false){
				ref1 = i - 1;
				break;
			}
		}
		j = ref;
		k = ref1;
		int count = k - j;
		while (count){
			mft.Records[j] = mft.Records[j + 1];
			count--;
			j++;
		}
		mft.Records[ref1] = temp;
		updateMFT();
	}

	unsigned long long int nullBlock(unsigned long long int ref){
		block bl;
		block temp;
		unsigned long long int nextRef;
		file.seekg(ref, file.beg);
		file.read((char*)&bl, sizeof(block));
		nextRef = bl.referenceToNextBlock;
		file.seekg(ref, file.beg);
		file.write((char*)&temp, sizeof(block));
		return nextRef;
	}

	void deleteFileRecords(unsigned long long int startingOffset, unsigned long long int blocksCount){
		unsigned long long int nextBlock = startingOffset;
		for (int i = 0; i < blocksCount; i++){
			nextBlock = nullBlock(nextBlock);
		}
	}

	void deleteFile(){
		string pathToFile;
		record temp;
		int ref;
		unsigned long long int startingOffset;
		unsigned long long int blocksCount;
		cout << "Enter path to file: ";
		cin >> pathToFile;
		ref = fileWasFound(pathToFile);
		if (ref != -1){
			startingOffset = mft.Records[ref].startingOffSet;
			blocksCount = mft.Records[ref].blocksCount;
			leftShift(ref);
			deleteFileRecords(startingOffset, blocksCount);
		}
		else{
			cout << fnf;
		}
	}

	unsigned long long int nextFreeBlock(unsigned long long int nextBlockRef){
		block temp;
		
		unsigned long long int returnedValue = nextBlockRef;
		file.seekg(nextBlockRef, file.beg);
		for (unsigned long long int i = 0; i < (endingOffset - nextBlockRef) / sizeof(block); i++){
			file.read((char*)&temp, sizeof(block));
			if (temp.referenceToNextBlock == default){
				return returnedValue;
			}
			returnedValue += sizeof(block);
		}
		return -1;
	}

	void writeFile(string fileInformation, int blockCount){
		block bl;
		unsigned long long int nextBlockRef = sizeof(masterFileTable);
		unsigned long long int temp;
		int j = 0;
		int count;
		nextBlockRef = nextFreeBlock(nextBlockRef);
		for (int i = 0; i < blockCount; i++){
			count = 0;
			while (fileInformation[j] != '\0'){
				bl.inform[count] = fileInformation[j];
				count++;
				if (count == 9){
					count++;
					bl.inform[count] = '\0';
					j++;
					break;
				}
				j++;
				if (fileInformation[j] == '\0'){
					bl.inform[count] = '\0';
					break;
				}
			}
			temp = nextFreeBlock(nextBlockRef + sizeof(block));
			bl.referenceToNextBlock = temp;
			if (i + 1 == blockCount){
				bl.referenceToNextBlock = -1;
			}
			file.seekg(nextBlockRef, file.beg);
			file.write((char*)&bl, sizeof(block));
			nextBlockRef = temp;
			
		}
	}

	void addNewFile(){
		string pathToFile;
		string fileInformation;
		record fileRecord;
		int blockCount;
		int j = 0;
		int count;
		block bl;
		cout << "Enter full path to file: ";
		cin.clear();
		cin >> pathToFile;
		if (fileWasFound(pathToFile)==-1){
			cout << "Enter file information: ";
			cin.clear();
			cin >> fileInformation;
			blockCount = fileInformation.size()/blockSize;
			if (fileInformation.size() % (blockSize-1)){
				blockCount++;
			}
			fileRecord.isUsed = true;
			fileRecord.pathToFile = pathToFile;
			fileRecord.startingOffSet = mft.ReferenseToLastFreeBlock;	
			fileRecord.blocksCount = blockCount;
			if ((endingOffset-mft.ReferenseToLastFreeBlock)/sizeof(block)>=blockCount){
				for (int i = 0; i < blockCount; i++){
					count = 0;
					while (fileInformation[j] != '\0'){
						bl.inform[count] = fileInformation[j];
						count++;
						if (count == 9){
							count++;
							bl.inform[count] = '\0';
							j++;
							break;
						}
						j++;
						if (fileInformation[j] == '\0'){
							bl.inform[count] = '\0';
							break;
						}
					}
					file.seekg(mft.ReferenseToLastFreeBlock, file.beg);
					bl.referenceToNextBlock = mft.ReferenseToLastFreeBlock + sizeof(block);
					if (i + 1 == blockCount){
						bl.referenceToNextBlock = default;
					}
					file.write((char*)&bl, sizeof(block));
					mft.ReferenseToLastFreeBlock += sizeof(block);
				}
				updateMFT(fileRecord);
			}
			else{
				writeFile(fileInformation, blockCount);

			}
		}
		else{
			return;
		}
	}
	void readFile(){
		string pathToFile;
		block fileInfo;
		char *wer;
		int recordLength;
		int ref;
		cout << "Enter full path to file: ";
		cin >> pathToFile;
		ref = fileWasFound(pathToFile);
		if (ref!=-1){
			showRecord(ref);
			cout << endl;
			system("pause");
			return;
		}
		else{
			cout << "File was not found!!" << endl;
			system("pause");
		}
		return;
	}

	void showRecord(int i){
		block fileInfo;
		int temp = mft.Records[i].startingOffSet;
		do{
			file.seekg(temp, file.beg);
			file.read((char*)&fileInfo, sizeof(block));
			cout << fileInfo.inform;
			temp += sizeof(block);
		} while (fileInfo.referenceToNextBlock != default);
		cout << endl;
	}

	void showAll(){
		for (int i = 0; mft.Records[i].isUsed == true; i++){
			cout << "File: " << mft.Records[i].pathToFile<<endl;
			showRecord(i);
		}
	}

	block readNextBlock(unsigned long long int BLOCK){
		block temp;
		file.seekg(BLOCK, file.beg);
		file.read((char*)&temp, sizeof(block));
		return temp;
	}

	void renewBlock(block bl, unsigned long long int place){
		file.seekg(place, file.beg);
		file.write((char*)&bl, sizeof(block));
	}

	void renewFile(){
		string fileName;
		string newFileINfo;
		block temp;
		unsigned long long int *blockArray;
		unsigned long long int BLOCK;
		unsigned long long int newBlockCount;
		block *array;
		int result;
		cout << "Enter file name: ";
		cin >> fileName;
		result = fileWasFound(fileName);
		if (result != -1){
			showRecord(result);
			cout << endl;
			cout << "Enter new Information: ";
			cin >> newFileINfo;
			newBlockCount = newFileINfo.size() / blockSize;
			if (newFileINfo.size() % (blockSize - 1)){
				newBlockCount++;
			}
			blockArray = (unsigned long long int*)malloc(mft.Records[result].blocksCount*sizeof(unsigned long long int));
			array = (block*)malloc(sizeof(block)*mft.Records[result].blocksCount);
			BLOCK = mft.Records[result].startingOffSet;
			for (int i = 0; i < mft.Records[result].blocksCount; i++){
				array[i] = readNextBlock(BLOCK);
				blockArray[i] = BLOCK;
				BLOCK = array[i].referenceToNextBlock;
			}
			int j = 0;
			int i;
			int count = 0;
			if (newBlockCount <= mft.Records[result].blocksCount){
				for ( i = 0; i < newBlockCount; i++){
					count = 0;
					while (newFileINfo[j] != '\0'){
						array[i].inform[count] = newFileINfo[j];
						count++;
						if (count == 9){
							count++;
							array[i].inform[count] = '\0';
							j++;
							break;
						}
						j++;
						if (newFileINfo[j] == '\0'){
							array[i].inform[count] = '\0';
							break;
						}
					}


				}
				array[i].referenceToNextBlock = -1;
				unsigned long long int temp2 = mft.Records[result].startingOffSet;
				for (int k = i + 1; k < mft.Records[result].blocksCount; k++){
					renewBlock(temp, temp2);
					temp2 = array[k].referenceToNextBlock;
				}
				unsigned long long int temp1 = mft.Records[result].startingOffSet;
				mft.Records[result].blocksCount = newBlockCount;
				for (int i = 0; i < newBlockCount; i++){
					if (i + 1 == newBlockCount){
						array[i].referenceToNextBlock = -1;
					}
					renewBlock(array[i], temp1);
					temp1 = array[i].referenceToNextBlock;
				}
				updateMFT();
				free(array);
				free(blockArray);
				return;
			}
			else{
				block *array2;
				array2 = (block*)malloc(sizeof(block)*newBlockCount);
				for (int k = 0; k < mft.Records[result].blocksCount; k++){
					array2[k] = array[k];
				}
				unsigned long long int temp3 = mft.ReferenseToLastFreeBlock;
				for (int k = mft.Records[result].blocksCount; k < newBlockCount; k++){
					array2[k].referenceToNextBlock = mft.ReferenseToLastFreeBlock;
					mft.ReferenseToLastFreeBlock += sizeof(block);
				}
				for (i = 0; i < newBlockCount; i++){
					count = 0;
					while (newFileINfo[j] != '\0'){
						array2[i].inform[count] = newFileINfo[j];
						count++;
						if (count == 9){
							count++;
							array2[i].inform[count] = '\0';
							j++;
							break;
						}
						j++;
						if (newFileINfo[j] == '\0'){
							array2[i].inform[count] = '\0';
							break;
						}
					}
				}
				for (int k = 0; k < newBlockCount; k++){
					cout << array2[k].inform << endl;
				}
				unsigned long long int temp4 = mft.Records[result].startingOffSet;
				for (int k = 0; k < newBlockCount; k++){
					if (k + 1 == newBlockCount){
						array2[k].referenceToNextBlock = -1;
					}
					renewBlock(array2[k], temp4);
					temp4 = array2[k].referenceToNextBlock;
				}
				cout << "---------------------------------" << endl;
				for (int k = 0; k < newBlockCount; k++){
					cout << array2[k].inform << endl;
				}
				mft.Records[result].blocksCount = newBlockCount;
				updateMFT();
				system("pause");
				return;

			}

		}
		else{
			cout << "File was not found!!" << endl;
			system("pause");
		}

	}
};

int main(){
	FS myDisk;

	char choice;
	char *inf;

	int i = sizeof(block);
	int j = sizeof(masterFileTable);

	inf = (char*)malloc(3675);

	i = sizeof(inf);

	while (1){
		menu();

		choice = getchar();
		getchar();
		//system("cls");

		switch (choice){
		case '1':
			myDisk.formatDisk();
			break;
		case '2':
			myDisk.addNewFile();
			break;
		case '3':
			myDisk.deleteFile();
			break;
		case '4':
			myDisk.readFile();
			break;
		case '5':
			myDisk.showAll();
			break;
		case '6':
			myDisk.renewFile();
			break;
		case '7':
			return 0;
		}
	}
	//cout << '\n';
	
	return 0;
}
