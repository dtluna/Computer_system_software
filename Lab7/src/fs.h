#include "structs.h"

#ifndef DISK
#define DISK "./disk.dsk"
#endif

#ifndef BUF_SIZE
#define BUF_SIZE 4096
#endif

mft_t mft = mft_init;

void init_fs ();

void add_file();
void delete_file();
void read_file();

ssize_t find_next_free_block(size_t i);

