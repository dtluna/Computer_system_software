#include "my_stdio.h"
#include "fs.h"

void init_fs ()
{
	FILE* file = fopen(DISK, "r+");
	if (NULL  == file)
	{
		file = fopen(DISK, "w");
		file_close(file);
	}
	else
	{
		fread(&mft, sizeof(char), sizeof(mft_t), file);
		file_close(file);
	}
}

void add_file ()
{
	char path[MAX_PATH_LEN] = "";
	puts("Enter file path:");
	get_string(path, MAX_PATH_LEN);

	char buffer[BUF_SIZE] = "";
	size_t bytes_read = 0;
	size_t j = 0;

	FILE* infile = file_open(path, "r");
	FILE* disk_file = file_open(DISK, "r+");

	for (size_t i = 0; i < MAX_BLOCKS; ++i)
	{
		bytes_read = fread(buffer, 1, BUF_SIZE, infile);

		j = find_next_free_block(i);
		if (j >= mft.blocks_count)
		{
			mft.blocks[j].is_used = true;
			mft.blocks_count++;
			mft.blocks[j].data_length = bytes_read;

			if (j == 0)
				mft.blocks[j].data_offset = sizeof(mft_t);
			else
				mft.blocks[j].data_offset = mft.blocks[j-1].data_offset + mft.blocks[j-1].data_length;
			file_seek(disk_file, mft.blocks[j].data_offset, SEEK_SET);
			fwrite(buffer, 1, bytes_read, disk_file);

			if(feof(infile))
			{
				file_close(infile);
				file_close(disk_file);
				return;
			}
		}
		else
		{

		}
		mft.blocks[j].is_used = true;
		mft.blocks[j].data_length = bytes_read;
	}

	
}

size_t find_next_free_block(size_t i)
{
	for (; i < MAX_BLOCKS; ++i)
		if (false == mft.blocks[i].is_used)
			return i;
}