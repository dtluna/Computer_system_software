typedef char bool;
#ifndef false
#define false 0
#endif

#ifndef true
#define true !false
#endif

#ifndef MAX_BLOCKS
#define MAX_BLOCKS 256*256
#endif

#ifndef MAX_RECORDS
#define MAX_RECORDS 256
#endif

#ifndef MAX_BLOCKS_PER_FILE
#define MAX_BLOCKS_PER_FILE 256
#endif

#ifndef MAX_PATH_LEN
#define MAX_PATH_LEN 256
#endif

struct block
{
	bool is_used;
	size_t data_length; //in bytes
	ssize_t data_offset; //offset of first byte of block on disk
};

typedef struct block block_t;

#ifndef block_init
#define block_init {.is_used = false, .data_length = 0, .data_offset = -1}
#endif


struct file_record
{
	char path[MAX_PATH_LEN];
	size_t size;
	ssize_t block_numbers[MAX_BLOCKS_PER_FILE];
	size_t n_blocks;
};

typedef struct file_record filerec_t;

#ifndef filerec_init
#define filerec_init {.path = "", .size = 0, .n_blocks = 0, .block_numbers = {[0...MAX_BLOCKS_PER_FILE-1] = -1}}
#endif

struct master_file_table
{
	size_t files_count;
	filerec_t records[MAX_RECORDS];
	size_t blocks_count;
	block_t blocks[MAX_BLOCKS_COUNT];
};

typedef master_file_table mft_t;

#ifndef mft_init
#define mft_init {.files_count = 0, .records = {[0...MAX_RECORDS-1] = filerec_init},
 .blocks_count = 0, .blocks = {[0...MAX_BLOCKS-1] = block_init}}
#endif
