#include "alloc.h"
#include "errors.h"
#include <stdlib.h>

char** alloc_str_array (size_t nmemb)
{
	char** str_array = (char**)calloc(nmemb, sizeof(char*));
	malloc_err(str_array);
	return str_array;
}

char* alloc_str (size_t length)
{
	char* string = (char*)calloc(length, sizeof(char));
	malloc_err(string);
	return string;
}

