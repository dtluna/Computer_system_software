#include "errors.h"
#include <stdlib.h>

void fopen_err(FILE* ret_val)//NULL & errno
{
	if (NULL == ret_val)
	{
		perror("fopen");
		exit(EXIT_FAILURE);
	}
}

void fclose_err(int ret_val)//EOF & errno
{
	if (EOF == ret_val)
	{
		perror("fclose");
		exit(EXIT_FAILURE);
	}
}

void fseek_err(int ret_val)//-1 & errno
{
	if (-1 == ret_val)
	{
		perror("fseek");
		exit(EXIT_FAILURE);
	}
}

void fgets_err(char* ret_val) //NULL
{
	if (NULL == ret_val)
	{
		perror("fgets");
		exit(EXIT_FAILURE);
	}
}

void malloc_err(void* ret_val)//NULL
{
	if (NULL == ret_val)
	{
		perror("malloc");
		exit(EXIT_FAILURE);
	}
}
