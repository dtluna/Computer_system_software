#include "structs.h"

mft_t mft;

void init_file_system();

void format_disk();

int find_file( string_t path);

void update_mft_rec( record_t file_record);

void update_mft();

void left_shift( int ref);

unsigned long long null_block(unsigned long long ref);

void delete_file_records(unsigned long long starting_offset, unsigned long long blocks_count);

void delete_file();

unsigned long long int next_free_block(unsigned long long next_block_ref);

void write_file(string_t file_info, int block_count);

void add_new_file();

void read_file();

void show_record( int i);

void show_all();

block_t read_next_block(unsigned long long block);

void renew_block(block_t block, unsigned long long int place);

void renew_file();
