#ifndef STRUCTS_H
#define STRUCTS_H

#define N_RECORDS 20
#define BLOCK_SIZE 10
#define ENDING_OFFSET 10485744
#define FNF "FILE not found.\n"
#define DEFAULT -1
#define MAX_PATH_LEN 256
#define DISK "./disk.dsk"

typedef char bool;
#ifndef false
#define false 0
#endif

#ifndef true
#define true !false
#endif

typedef char* string_t;

struct record
{
	char path_to_file[MAX_PATH_LEN];
	unsigned long long starting_offset;
	unsigned long long blocks_count;
	bool is_used;
};

typedef struct record record_t;

#ifndef record_t_init
#define record_t_init {.starting_offset = 0, .blocks_count = 0, .is_used = false, .path_to_file = ""}
#endif

struct master_file_table
{
	record_t records[N_RECORDS];
	unsigned long long full_records_count;
	unsigned long long ref_to_last_free_block;
};

typedef struct master_file_table mft_t;

#ifndef mft_t_init
#define mft_t_init {.full_records_count = 0, .ref_to_last_free_block = sizeof(mft_t), .records = {[0 ... N_RECORDS-1] = record_t_init}}
#endif

struct block
{
	char info[BLOCK_SIZE];
	unsigned long long ref_to_next_block;
};

typedef struct block block_t;

#ifndef block_t_init
#define block_t_init {.info = "", .ref_to_next_block = DEFAULT}
#endif


#endif