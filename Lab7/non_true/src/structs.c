#include "structs.h"
#include <string.h>

#ifndef empty_string
#define empty_string(string, size) strncpy(string, "", size)
#endif

/*
void init_record(record_t* rec)
{
	rec->starting_offset = 0;
	rec->blocks_count = 0;
	rec->is_used = false;
	empty_string(rec->path_to_file, MAX_PATH_LEN);
}

void init_mft(mft_t* mft)
{
	mft->full_records_count = 0;
	mft->ref_to_last_free_block = sizeof(mft_t);
	for (int i = 0; i < N_RECORDS; ++i)
		init_record(&(mft->records[i]));
	
}
*/