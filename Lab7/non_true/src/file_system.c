#include "my_stdio.h"
#include "file_system.h"
#include <string.h>
#include <stdlib.h>

void init_file_system()
{
	FILE* file = fopen(DISK, "a+");
	if (NULL  == file)
	{
		file = fopen(DISK, "w");
		file_close(file);
		file = file_open(DISK, "r");
	}
	file_seek(file, 0, SEEK_SET);
	fread(&mft, sizeof(char), sizeof(mft_t), file);
	file_close(file);
}

void format_disk()
{
	block_t block = block_t_init;

	FILE* file = file_open(DISK, "w");

	mft_t new_mft =  mft_t_init;

	fwrite(&new_mft, sizeof(char), sizeof(mft_t), file);

	for (int i = 0; i < (ENDING_OFFSET - sizeof(mft_t))/24; i++)
		fwrite(&block, sizeof(char), sizeof(block_t), file);
	
	mft = new_mft;
	file_close(file);
}

int find_file(string_t path)
{
	for (int i = 0; i < N_RECORDS; i++)
		if (path == mft.records[i].path_to_file)
			return i;
	return -1;
}

void update_mft_rec(record_t file_record)
{
	for (int i = 0; i < N_RECORDS; i++)
		if (mft.records[i].is_used == false)
		{
			mft.records[i] = file_record;
			FILE* file = file_open(DISK, "w");
			fwrite(&mft, sizeof(char), sizeof(mft_t), file);
			file_close(file);
			break;
		}
}

void update_mft()
{
	FILE* file = file_open(DISK, "w");
	fwrite(&mft, sizeof(char), sizeof(mft_t), file);
	file_close(file);
}

void left_shift( int ref)
{
	int ref1;
	int j, k;
	record_t temp;
	for (int i = 0; i < N_RECORDS; i++)
		if (mft.records[i].is_used == false){
			ref1 = i - 1;
			break;
		}
	
	j = ref;
	k = ref1;
	int count = k - j;
	while (count)
	{
		mft.records[j] = mft.records[j + 1];
		count--;
		j++;
	}
	mft.records[ref1] = temp;
	update_mft();
}

unsigned long long null_block(unsigned long long ref)
{
	block_t block = block_t_init;
	block_t temp = block_t_init;
	unsigned long long next_ref;

	FILE* file = file_open(DISK, "r+");
	file_seek(file, ref, SEEK_SET);
	fread(&block, sizeof(char), sizeof(block_t), file);

	next_ref = block.ref_to_next_block;

	file_seek(file, ref, SEEK_SET);
	fwrite(&temp, sizeof(char), sizeof(block_t), file);
	file_close(file);
	return next_ref;
}

void delete_file_records(unsigned long long starting_offset, unsigned long long blocks_count)
{
	unsigned long long next_block = starting_offset;
		for (int i = 0; i < blocks_count; i++)
			next_block = null_block(next_block);
}

void delete_file()
{
	char path_to_file[MAX_PATH_LEN];

	printf("Enter path to file: ");
	get_string(path_to_file, MAX_PATH_LEN);

	int ref = find_file(path_to_file);

	if (ref != -1)
	{
		unsigned long long starting_offset = mft.records[ref].starting_offset;
		unsigned long long blocks_count = mft.records[ref].blocks_count;
		left_shift(ref);
		delete_file_records(starting_offset, blocks_count);
	}
	else
		fprintf(stderr, "Error: file '%s' not found.\n", path_to_file);
	
}

unsigned long long next_free_block(unsigned long long next_block_ref)
{
	block_t temp = block_t_init;
		
	unsigned long long ret_val = next_block_ref;

	FILE* file = file_open(DISK, "r");
	file_seek(file, next_block_ref, SEEK_SET);

	for (unsigned long long i = 0; i < (ENDING_OFFSET - next_block_ref) / sizeof(block_t); i++)
	{
		fread(&temp, sizeof(char), sizeof(block_t), file);
		if (temp.ref_to_next_block == -1)
		{
			file_close(file);
			return ret_val;
		}
		ret_val += sizeof(block_t);
	}
	file_close(file);
	return -1;
}

void write_file(string_t file_info, int block_count)
{
	block_t block = block_t_init;
	unsigned long long next_block_ref = sizeof(mft_t);
	unsigned long long temp;
	int j = 0;
	int count;
	next_block_ref = next_free_block(next_block_ref);
	for (int i = 0; i < block_count; i++)
	{
		count = 0;
		while (file_info[j] != '\0')
		{
			block.info[count] = file_info[j];
			count++;

			if (count == 9)
			{
				count++;
				block.info[count] = '\0';
				j++;
				break;
			}

			j++;
			if (file_info[j] == '\0')
			{
				block.info[count] = '\0';
				break;
			}
		}
		temp = next_free_block(next_block_ref + sizeof(block_t));
		block.ref_to_next_block = temp;

		if (i + 1 == block_count)
			block.ref_to_next_block = -1;
		
		FILE* file = file_open(DISK, "a");
		file_seek(file, next_block_ref, SEEK_SET);
		fwrite(&block, sizeof(char), sizeof(block_t), file);
		file_close(file);

		next_block_ref = temp;	
	}
}

void add_new_file()
{
	char path_to_file[MAX_PATH_LEN];
	char file_info[BLOCK_SIZE];
	record_t file_record = record_t_init;
	int block_count;
	int j = 0;
	int count;
	block_t block = block_t_init;

	printf("Enter path to file: ");
	get_string(path_to_file, MAX_PATH_LEN);

	if (-1 == find_file(path_to_file))
	{
		printf("Enter file information: ");
		get_string(file_info, MAX_PATH_LEN);
		block_count = strlen(file_info)/BLOCK_SIZE; 

		if (0 != (strlen(file_info) % (BLOCK_SIZE-1)) )
			block_count++;
		
		file_record.is_used = true;
		strcpy(file_record.path_to_file, path_to_file);
		file_record.starting_offset = mft.ref_to_last_free_block;	
		file_record.blocks_count = block_count;

		if ( (ENDING_OFFSET-mft.ref_to_last_free_block)/sizeof(block_t) >= block_count)
		{
			for (int i = 0; i < block_count; i++)
			{
				count = 0;
				while (file_info[j] != '\0')
				{
					block.info[count] = file_info[j];
					count++;

					if (count == 9)
					{
						count++;
						block.info[count] = '\0';
						j++;
						break;
					}

					j++;

					if (file_info[j] == '\0')
					{
						block.info[count] = '\0';
						break;
					}
				}

				FILE* file = file_open(DISK, "a");
				file_seek(file, mft.ref_to_last_free_block, SEEK_SET);
				block.ref_to_next_block = mft.ref_to_last_free_block + sizeof(block_t);

				if (i + 1 == block_count)
					block.ref_to_next_block = -1;
				
				fwrite(&block, sizeof(char), sizeof(block_t), file);
				mft.ref_to_last_free_block += sizeof(block_t);

				file_close(file);
			}
			update_mft_rec(file_record);
		}
		else
			write_file(file_info, block_count);
		
	}
	else
		return;
}

void read_file()
{
	char path_to_file[MAX_PATH_LEN];
	int ref;

	printf("Enter full path to file:");
	get_string(path_to_file, MAX_PATH_LEN);

	ref = find_file(path_to_file);
	if (ref!=-1)
	{
		show_record(ref);//можно ещё 1 newline
		system("pause");
		return;
	}
	else
	{
		puterrs("read_file: file was not found!");
		system("pause");
		return;
	}
	return;
}

void show_record( int i)
{
	block_t file_info = block_t_init;
	int temp = mft.records[i].starting_offset;
	FILE* file = file_open(DISK, "r");

	do
	{
		file_seek(file, temp, SEEK_SET);
		fread(&file_info, sizeof(char), sizeof(block_t), file);
		puts(file_info.info);
		temp += sizeof(block_t);
	} while (file_info.ref_to_next_block != -1);

	file_close(file);
	putchar('\n');
}

void show_all()
{
	for (int i = 0; i < N_RECORDS, mft.records[i].is_used == true; i++)
	{
		printf("File:%s\n", mft.records[i].path_to_file);
		show_record(i);
	}
}

block_t read_next_block(unsigned long long block)
{
	block_t temp = block_t_init;
	FILE* file = file_open(DISK, "r");
	file_seek (file, block, SEEK_SET);
	fread(&temp, sizeof(char), sizeof(block_t), file);
	file_close(file);
	return temp;
}

void renew_block(block_t block, unsigned long long int place)
{
	FILE* file = file_open(DISK, "a");
	file_seek (file, place, SEEK_SET);
	fwrite(&block, sizeof(char), sizeof(block_t), file);
	file_close(file);
}

void renew_file()
{
	char filename[MAX_PATH_LEN];
	char new_file_info[MAX_PATH_LEN];
	block_t temp = block_t_init;
	unsigned long long int *block_array;
	unsigned long long int block;
	unsigned long long int new_block_count;
	block_t *array;
	int result;

	printf("Enter file name:");
	get_string(filename, MAX_PATH_LEN);
	result = find_file(filename);

	if (result != -1)
	{
		show_record(result);
		printf("Enter new information:");
		get_string(new_file_info, MAX_PATH_LEN);

		new_block_count = strlen(new_file_info) / BLOCK_SIZE;
		if (strlen(new_file_info) % (BLOCK_SIZE - 1))
			new_block_count++;
		
		block_array = (unsigned long long int*)calloc(mft.records[result].blocks_count, sizeof(unsigned long long));
		array = (block_t*)calloc(mft.records[result].blocks_count, sizeof(block_t));

		block = mft.records[result].starting_offset;
		for (int i = 0; i < mft.records[result].blocks_count; i++){
			array[i] = read_next_block(block);
			block_array[i] = block;
			block = array[i].ref_to_next_block;
		}

		int j = 0;
		int i;
		int count = 0;
		if (new_block_count <= mft.records[result].blocks_count)
		{
			for ( i = 0; i < new_block_count; i++)
			{
				count = 0;
				while (new_file_info[j] != '\0')
				{
					array[i].info[count] = new_file_info[j];
					count++;

					if (count == 9)
					{
						count++;
						array[i].info[count] = '\0';
						j++;
						break;
					}
					j++;
					if (new_file_info[j] == '\0'){
						array[i].info[count] = '\0';
						break;
					}
				}

			}
			array[i].ref_to_next_block = -1;
			unsigned long long int temp2 = mft.records[result].starting_offset;

			for (int k = i + 1; k < mft.records[result].blocks_count; k++)
			{
				renew_block(temp, temp2);
				temp2 = array[k].ref_to_next_block;
			}

			unsigned long long int temp1 = mft.records[result].starting_offset;
			mft.records[result].blocks_count = new_block_count;

			for (int i = 0; i < new_block_count; i++)
			{
				if (i + 1 == new_block_count)
					array[i].ref_to_next_block = -1;
				
				renew_block(array[i], temp1);
				temp1 = array[i].ref_to_next_block;
			}

			update_mft();
			free(array);
			free(block_array);
			return;
		}

		else
		{
			block_t *array2;
			array2 = (block_t*)calloc(new_block_count, sizeof(block_t));

			for (int k = 0; k < mft.records[result].blocks_count; k++)
				array2[k] = array[k];
			
			for (int k = mft.records[result].blocks_count; k < new_block_count; k++)
			{
				array2[k].ref_to_next_block = mft.ref_to_last_free_block;
				mft.ref_to_last_free_block += sizeof(block);
			}

			for (i = 0; i < new_block_count; i++)
			{
				count = 0;
				while (new_file_info[j] != '\0')
				{
					array2[i].info[count] = new_file_info[j];
					count++;
					if (count == 9)
					{
						count++;
						array2[i].info[count] = '\0';
						j++;
						break;
					}

					j++;

					if (new_file_info[j] == '\0')
					{
						array2[i].info[count] = '\0';
						break;
					}
				}
			}

			for (int k = 0; k < new_block_count; k++)
				puts(array2[k].info);
			
			unsigned long long int temp3 = mft.records[result].starting_offset;
			for (int k = 0; k < new_block_count; k++)
			{
				if (k + 1 == new_block_count)
				{
					array2[k].ref_to_next_block = -1;
				}
				renew_block(array2[k], temp3);
				temp3 = array2[k].ref_to_next_block;
			}

			puts( "---------------------------------");
			for (int k = 0; k < new_block_count; k++)
				puts(array2[k].info);
			
			mft.records[result].blocks_count = new_block_count;
			update_mft();
			system("pause");
			return;
			}
		}
	else
	{
		puterrs("renew_file:file was not found!");
		system("pause");
	}
}
