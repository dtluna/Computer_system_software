#include <stdio.h>
#include <stdlib.h>
#include "file_system.h"

#ifndef remove_newline
#define remove_newline() getchar()
#endif

void print_menu();

int main(int argc, char const *argv[])
{
	init_file_system();
	
	char choice = 0;

	while(1)
	{	
		print_menu();
		choice = getchar();
		remove_newline();

		switch(choice)
		{
			case '1': 
				format_disk();
				break;

			case '2': 
				add_new_file();
				break;

			case '3': 
				delete_file();
				break;

			case '4': 
				read_file();
				break;

			case '5': 
				show_all();
				break;

			case '6': 
				renew_file();
				break;

			case '7': 
				exit(EXIT_SUCCESS);
				break;	
		}
		puts("");
	}
	return 0;
}

void print_menu()
{
	puts("--------------");
	puts("1.Format disk");
	puts("2.Add new file");
	puts("3.Delete file");
	puts("4.Show file");
	puts("5.Show all");
	puts("6.Renew file");
	puts("7.Exit");
	puts("--------------");
}
