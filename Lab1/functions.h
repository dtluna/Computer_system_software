#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

void show (int* const array, int size);
void swap (int* src, int* dest);
int* sort(int* array, int size);
int* convert(int argc, char const *argv[]);
