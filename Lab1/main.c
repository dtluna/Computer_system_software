#include "functions.h"

int main(int argc, char const *argv[])
{	
	if (argc < 2)
	{
		fprintf(stderr, "Usage: %s <integers>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	int* array = convert(argc, argv);
	
	pid_t pid = fork();
	switch(pid)
	{
		case -1: 
			perror("fork");
			exit(EXIT_FAILURE);
			break;
		case 0: 
			//printf("This is the child process. PID:%d\n", pid); 
			array = sort(array, argc-1); 
			show(array, argc-1); 
			break;
		default: 
			//printf("Child process created successfully. PID:%d\n", pid); 
			break;
	}

	return 0;
}
