#include "functions.h"

void show (int* const array, int size)
{
	for (int i = 0; i < size; i++)
	{
		printf("a[%d] = %d\n",i, array[i] );
	}
}

void swap (int* src, int* dest)
{
	int temp = *src;
	*src = *dest;
	*dest = temp;
}

int* sort(int* array, int size)
{
	int temp;
	for (int i = 0; i < size; ++i)
	{
		for (int j = size - 1; j > i; --j)
		{
			if (array[i] > array[j])
			{
				swap(&array[i], &array[j]);
			}
		}
	}
	return array;
}

int* convert(int argc, char const *argv[])
{
	int* array = (int*)calloc(argc-1, sizeof(int));

	for (int i = 0; i < argc-1; i++)
	{
		array[i] = atoi(argv[i+1]);
	}

	return array;
}
