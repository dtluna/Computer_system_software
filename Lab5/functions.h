#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <errno.h>
#include <pthread.h>
#include <dlfcn.h>
#include <aio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

#define NUM_OF_FILES 64
#define BUF_SIZE 4096
#define FIFO_NAME "./lab5fifo"
#define RM_FIFO remove(FIFO_NAME);
#define DEST_NAME "dest/dest.txt"
#define FIN_WRITE_SEM_NAME "/finished_writing"
#define FIN_READ_SEM_NAME "/finished_reading"
#define SHARED_LIB_NAME "./async_rw.so"

struct reader_arguments
 	{
 		const char* src_path;
 		char* buffer;
 	};

void create_fifo(const char *pathname, mode_t mode);
void check_write(ssize_t return_value);
void check_read (ssize_t return_value);
void check_open(int return_value);

void file_check(FILE* return_value, const char* path, const char* message);
FILE* open_file(const char* path, const char* mode);
void clear_buffer(char* buffer);

void create_thread	(pthread_t *thread,
					const pthread_attr_t *attr,
					void *(*start_routine) (void *),
					void *arg);

void thread_check(int return_value, const char* message);
char** listdir (const char* path, size_t *num_of_files);
char** allocate_string_array(size_t nmemb);
char* allocate_string(size_t length);
char** reallocate_string_array(char** string_array, size_t nmemb);

void* reader_func (void* args);
void* writer_func (void* args);
struct aiocb initialise_aio_cb ();

sem_t* open_semaphore (const char *name, int oflag,
					mode_t mode, unsigned int value);
void semaphore_check (int return_value, char* func_name);
void semaphore_post(sem_t* semaphore);
void semaphore_wait(sem_t* semaphore);
void semaphore_close(sem_t* semaphore);
