#include "functions.h"

void create_thread	(pthread_t *thread,
					const pthread_attr_t *attr,
					void *(*start_routine) (void *),
					void *arg)
{
	thread_check(pthread_create(thread,attr,start_routine,arg), "pthread_create");
}

void create_fifo(const char *pathname, mode_t mode)
{
	if (-1 == mkfifo(pathname, mode))
	{
		perror("mkfifo");
		RM_FIFO
		exit(EXIT_FAILURE);
	}
}

void* reader_func (void* args)
{
	sem_t* finished_reading = open_semaphore(FIN_WRITE_SEM_NAME, O_CREAT, 0666, 0);
	sem_t* finished_writing = open_semaphore(FIN_READ_SEM_NAME, O_CREAT, 0666, 0);

	struct reader_arguments* read_args = (struct reader_arguments*)args;
	size_t num_of_files = 0;
	char** filenames = listdir(read_args->src_path, &num_of_files);

	int fifo_fd = open(FIFO_NAME, O_WRONLY);
	check_open(fifo_fd);
	if (-1 == dprintf(fifo_fd,"%d", num_of_files))
	{
		fprintf(stderr, "error: dprintf(fifo_fd,'%d', num_of_files)\n");
		RM_FIFO
		exit(EXIT_FAILURE);
	}

	void* library = dlopen(SHARED_LIB_NAME, RTLD_LAZY);
	if (!library) 
	{
		fprintf(stderr, "%s\n", dlerror());
		RM_FIFO
		exit(EXIT_FAILURE);
	}

	char *error;
	void (*async_read)(struct aiocb*) = dlsym(library, "async_read");
	if ((error = dlerror()) != NULL)  
	{
		fprintf(stderr, "%s\n", error);
		RM_FIFO
		exit(EXIT_FAILURE);
	}

	struct aiocb source_cb = initialise_aio_cb();
	source_cb.aio_buf = read_args->buffer;

	char path[512];
	for (int i = 0; i < num_of_files; i++)
	{
		strcpy(path, read_args->src_path);
		strcpy(path,strcat(path,"/"));
		clear_buffer(source_cb.aio_buf);
		source_cb.aio_fildes = fileno(open_file(strcat(path,filenames[i]), "r"));
		async_read(&source_cb);
		//printf("[%d]reading:%s\n%s\n", i,path,source_cb.aio_buf);
		//printf("[%d]reading:+r\n", i);
		semaphore_post(finished_reading);
		//printf("[%d]reading:-w\n", i);
		semaphore_wait(finished_writing); 
	}
	dlclose(library);
	return NULL;
}

void* writer_func (void* args)
{
	sem_t* finished_reading = open_semaphore(FIN_WRITE_SEM_NAME, O_CREAT, 0666, 0);
	sem_t* finished_writing = open_semaphore(FIN_READ_SEM_NAME, O_CREAT, 0666, 0);

	char* buffer = (char*) args;

	int fifo_fd = open(FIFO_NAME, O_RDONLY);
	check_open(fifo_fd);
	char temp[8];
	check_read(read(fifo_fd, temp, sizeof(temp)));
	size_t num_of_files = atoi(temp);

	void* library = dlopen(SHARED_LIB_NAME, RTLD_LAZY);
	if (!library) 
	{
		fprintf(stderr, "%s\n", dlerror());
		RM_FIFO
		exit(EXIT_FAILURE);
	}

	char *error;
	void (*async_write)(struct aiocb*) = dlsym(library, "async_write");
	if ((error = dlerror()) != NULL)  
	{
		fprintf(stderr, "%s\n", error);
		RM_FIFO
		exit(EXIT_FAILURE);
	}

	struct aiocb destination_cb = initialise_aio_cb();
	destination_cb.aio_buf = buffer;
	destination_cb.aio_fildes = fileno(open_file(DEST_NAME, "w"));

	for (int i = 0; i < num_of_files; i++)
	{
		//printf("[%d]writing:-r\n",i);
		semaphore_wait(finished_reading); 
		async_write(&destination_cb);
		//printf("[%d]writing:%s\n", i,buffer);
		destination_cb.aio_offset += strlen(buffer);
		//printf("[%d]writing:+w\n",i);
		semaphore_post(finished_writing);
	}
	semaphore_close(finished_reading);
	semaphore_close(finished_writing);
	dlclose(library);
	return NULL;
}

struct aiocb initialise_aio_cb ()
{
	struct aiocb cb;
	cb.aio_reqprio = 20;
	cb.aio_offset = 0;
	cb.aio_nbytes = BUF_SIZE;
	cb.aio_sigevent.sigev_notify = SIGEV_NONE;
	return cb;
}
	
char** listdir (const char* path, size_t *num_of_files)
{
	DIR *dp;
	struct dirent *ep;
	*num_of_files = NUM_OF_FILES;

	char** filenames = allocate_string_array(*num_of_files);

	dp = opendir(path);
	if (NULL != dp)
	{
		size_t i = 0;
		int my_errno = errno;
		while(ep = readdir (dp))
		{
			if (DT_REG == ep->d_type)
			{
				if (i == *num_of_files)
				{
					*num_of_files *= 2;
					filenames = reallocate_string_array(filenames, *num_of_files);
				}
				filenames[i] = ep->d_name;
				//puts(filenames[i]);
				i++;
			}
		}
		if (errno != my_errno)
		{
			perror("readdir");
			free(filenames);
			RM_FIFO
			exit(EXIT_FAILURE);
		}
		*num_of_files = i;
		char** filenames_final = allocate_string_array(*num_of_files);
		for (i = 0; i < *num_of_files; ++i)
		{
			filenames_final[i] = allocate_string(strlen(filenames[i]));
			strcpy(filenames_final[i], filenames[i]);
		}
		free(filenames);
		closedir (dp);
		return filenames_final;
	}
	else
	{
		perror("Couldn't open directory");
		free(filenames);
		RM_FIFO
		exit(EXIT_FAILURE);
	}	
}

char** allocate_string_array(size_t nmemb)
{
	char** string_array = (char**)calloc(nmemb,sizeof(char*));
	if (NULL == string_array)
	{
		perror("Memory allocation error.");
		RM_FIFO
		exit(EXIT_FAILURE);
	}
	return string_array;
}	

char* allocate_string(size_t length)
{
	char* string = (char*)calloc(length,sizeof(char));
	if (NULL == string)
	{
		perror("Memory allocation error.");
		RM_FIFO
		exit(EXIT_FAILURE);
	}
	return string;
}

char** reallocate_string_array(char** string_array, size_t nmemb)
{
	string_array = (char**)realloc(string_array, nmemb);
	if (NULL == string_array)
	{
		perror("Memory allocation error.");
		RM_FIFO
		exit(EXIT_FAILURE);
	}
	return string_array;
}

void thread_check(int return_value, const char* message)
{
	if (0 != return_value)
	{
		perror(message);
		RM_FIFO
		exit(EXIT_FAILURE);
	}
}

FILE* open_file(const char* path, const char* mode)
{
	FILE* file = fopen(path,mode);
	file_check(file, path,"fopen");
	return file;
}

void file_check(FILE* return_value, const char* path, const char* message)
{
	if (NULL == return_value)
	{
		fprintf(stderr, "%s:", path);
		perror(message);
		RM_FIFO
		exit(EXIT_FAILURE);
	}
}

void clear_buffer(char* buffer)
{
	for (int i = 0; i < BUF_SIZE; ++i)
	{
		buffer[i] = 0;
	}
}

void check_open(int return_value)
{
	if (-1 == return_value)
	{
		perror("open");
		RM_FIFO
		exit(EXIT_FAILURE);
	}
}

void check_write(ssize_t return_value)
{
	if (-1 == return_value)
	{
		perror("write");
		RM_FIFO
		exit(EXIT_FAILURE);
	}
}
 
void check_read (ssize_t return_value)
{
	if (-1 == return_value)
	{
		perror("read");
		RM_FIFO
		exit(EXIT_FAILURE);
	}
}

sem_t* open_semaphore (const char *name, int oflag,
					mode_t mode, unsigned int value)
{
	sem_t* semaphore = sem_open(name, oflag, mode, value);
	if (SEM_FAILED == semaphore)
	{
		perror("sem_open");
		RM_FIFO
		exit(EXIT_FAILURE);
	}
	return semaphore;
}

void semaphore_check (int return_value, char* func_name)
{
	if (0 != return_value)
	{
		perror(func_name);
		RM_FIFO
		exit(EXIT_FAILURE);
	}
}

void semaphore_post(sem_t* semaphore)
{
	semaphore_check(sem_post(semaphore), "sem_post");
}

void semaphore_wait(sem_t* semaphore)
{
	semaphore_check(sem_wait(semaphore), "sem_wait");
}

void semaphore_close(sem_t* semaphore)
{
	semaphore_check(sem_close(semaphore),"sem_close");
}
