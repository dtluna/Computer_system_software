#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <aio.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>


void async_read (struct aiocb* srccb_ptr);
void async_write (struct aiocb* destcb_ptr);

void async_read (struct aiocb* srccb_ptr)
{
	aio_read(srccb_ptr);
	int error = 0;
	while(error = aio_error(srccb_ptr))
	{
		if (EINPROGRESS == error)
			continue;
		if (ECANCELED == error)
		{
			fprintf(stderr, "The request was cancelled.\n");
			exit(EXIT_FAILURE);
		}
		if (error > 0)
		{
			perror("aio_read");
			exit(EXIT_FAILURE);
		}
	}
	//printf("strlen(srccb_ptr->aio_buf) = %d\n",strlen(srccb_ptr->aio_buf));
	//printf("srccb_ptr->aio_buf:%s\n", srccb_ptr->aio_buf);
}

void async_write (struct aiocb* destcb_ptr)
{
	destcb_ptr->aio_nbytes = strlen(destcb_ptr->aio_buf);

	aio_write(destcb_ptr);
	int error = 0;
	while(error = aio_error(destcb_ptr))
	{
		if (EINPROGRESS == error)
			continue;
		if (ECANCELED == error)
		{
			fprintf(stderr, "The request was cancelled.\n");
			exit(EXIT_FAILURE);
		}
		if (error > 0)
		{
			perror("aio_write");
			exit(EXIT_FAILURE);
		}
	}
	//printf("strlen(destcb_ptr->aio_buf) = %d\n",strlen(destcb_ptr->aio_buf));
	//printf("destcb_ptr->aio_buf:%s\n", destcb_ptr->aio_buf);
}
