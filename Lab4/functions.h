#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "thread_record.h"
#include "getch.h"
#include <time.h>

pthread_mutex_t mutex;
void add_thread (pthread_t* thread);
void close_last_thread (pthread_t* thread);
void* thread_func (void* unused);

void thread_check(int return_value, char* message);
