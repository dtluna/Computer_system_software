#include "functions.h"
//void * (*start_routine) (void *) — указатель на функцию, возвращающую void* и принимающую аргумент void*

/*Начальный процесс является управляющим.
Он принимает поток ввода с клавиатуры и контролирует дочерние процессы.
По нажатию клавиши ‘+’ добавляется новый процесс, 
‘-’ – удаляется последний добавленный, 
‘q’ – программа завершается. 
Каждый дочерний процесс посимвольно выводит на экран в вечном цикле свою уникальную строку. 
При этом операция вывода строки должна быть атомарной, 
т.е. процесс вывода должен быть синхронизирован таким образом, 
чтобы строки на экране не перемешивались. 
Выполняется в двух вариантах: под Linux и Windows. 
В качества метода синхронизации использовать сигналы/события.
*/

int main()
{
	char c = 0;
	int i = 0;
	
	struct thread_record pthr_rec;
	pthr_rec = initialize_thread_record(pthr_rec);

	printf("Main thread: %#.8x.\n", pthread_self());
	while(1)
	{
		c = getch();

		if('a' == c)
		{
			add_thread(&(pthr_rec.threads[pthr_rec.number_of_threads]));//создать тред
			pthr_rec.number_of_threads++;
		}

		if ('q' == c)
		{
			while(pthr_rec.number_of_threads)
			{
				close_last_thread(&(pthr_rec.threads[pthr_rec.number_of_threads-1]));
				pthr_rec.number_of_threads--;
			}

			exit(EXIT_SUCCESS);
		}

		if ('-' == c)
		{
			if (pthr_rec.number_of_threads > 0)
			{
				close_last_thread(&(pthr_rec.threads[pthr_rec.number_of_threads-1]));//убить один тред
				pthr_rec.number_of_threads--;
			}
			else
			{
				fprintf(stderr, "There are no created threads.\n");
			}
		}
	}
	return 0;
}
