#include "thread_record.h"

struct thread_record initialize_thread_record (struct thread_record pthr_rec)
{
	pthr_rec.number_of_threads = 0;
	for (int i = 0; i < THREAD_ARRAY_SIZE; ++i)
	{
		pthr_rec.threads[i] = -1;
	}
	return pthr_rec;
}