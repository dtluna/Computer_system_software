#include <pthread.h>
#ifndef THREAD_ARRAY_SIZE
#define THREAD_ARRAY_SIZE 256
#endif
struct thread_record
{
	pthread_t threads[THREAD_ARRAY_SIZE];
	unsigned short number_of_threads;
};

struct thread_record initialize_thread_record (struct thread_record pthr_rec);