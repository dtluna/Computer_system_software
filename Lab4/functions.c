#include "functions.h"
	
void add_thread (pthread_t* thread)
{

	thread_check(pthread_create(thread, NULL, &thread_func, NULL), "pthread_create");
	printf("Main: Created thread #%#.8x.\n", *thread);
}

void close_last_thread (pthread_t* thread)
{
	thread_check(pthread_mutex_lock(&mutex), "pthread_mutex_lock");
	thread_check(pthread_cancel(*thread), "pthread_cancel");
	thread_check(pthread_mutex_unlock(&mutex), "pthread_mutex_lock");
	printf("Main: Closed thread #%#.8x.\n", *thread);
}

void* thread_func (void* unused)
{
	char string[11] = "0123456789\0";
	int i = 0;
	while(1)
	{	
		//printf("Thread #%#.12x: locking mutex.\n", pthread_self());
		thread_check(pthread_mutex_lock(&mutex), "pthread_mutex_lock");
		printf("Thread #%#.8x:", pthread_self());
		i = 0;
		while(i < 11)
		{	
			printf("%c", string[i]);
			i++;
			fflush(stdout);
		}
		puts("");
		//printf("Thread #%#.12x: unlocking mutex.\n", pthread_self());
		thread_check(pthread_mutex_unlock(&mutex), "pthread_mutex_unlock");
		sleep(1);
	}
}

void thread_check(int return_value, char* message)
{
	if (0 != return_value)
	{
		perror(message);
		exit(EXIT_FAILURE);
	}
}
