#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

void server(int pipefd[2]);
void client(int pipefd[2]);
void semaphore_check(sem_t* return_value, char* process_name, char* sem_name);
