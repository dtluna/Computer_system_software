#include "functions.h"

void server(int pipefd[2])
{
	char string[256];

	close(pipefd[0]); //закрываем ненужный для чтения конец
	
	sem_t* ready_to_write = sem_open("/lab3_ready_to_write\0", O_CREAT, 0666, 0);
	semaphore_check(ready_to_write, "Parent\0", "/lab3_ready_to_write\0");

	sem_t* read_success = sem_open("/lab3_read_success\0", O_CREAT, 0666, 0);
	semaphore_check(read_success, "Parent\0", "/lab3_read_success\0");

	while(1)
	{
		printf("Server: Enter a string:\n");
		fgets(string, 256, stdin);
		write(pipefd[1], string, 256);
		sem_post(ready_to_write);//говорим клиенту, что готовы начать передачу данных
		sem_wait(read_success);//принимаем сообщение, что всё окей
	}
}

void client(int pipefd[2])
{
	char string[256];

	close(pipefd[1]);//закрываем ненужный для записи конец

	sem_t* ready_to_write = sem_open("/lab3_ready_to_write\0", O_CREAT, 0666, 0);
	semaphore_check(ready_to_write, "Child\0", "/lab3_ready_to_write\0");

	sem_t* read_success = sem_open("/lab3_read_success\0", O_CREAT, 0666, 0);
	semaphore_check(read_success, "Child\0", "/lab3_read_success\0");

	while(1)
	{
		sem_wait(ready_to_write);//принимаем сообщение о том, что готовы начать передачу данных
		read(pipefd[0], string, 256);
		printf("Client: your string is: %s\n", string);
		sem_post(read_success);//отправляем сообщение, что всё окей
	}
}

void semaphore_check(sem_t* return_value, char* process_name, char* sem_name)
{
	if (SEM_FAILED == return_value)
	{
		fprintf(stderr, "%s %s\n", process_name, sem_name);
		perror("sem_open");
		_exit(EXIT_FAILURE);
	}
}
