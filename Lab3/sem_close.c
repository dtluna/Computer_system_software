#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

void sem_unlink_check(int return_value)
{
	if(-1 == return_value)
	{
		perror("sem_unlink");
		_exit(EXIT_FAILURE);
	}
}

int main()
{
	sem_unlink_check(sem_unlink("/lab3_ready_to_write\0"));

	sem_unlink_check(sem_unlink("/lab3_read_success\0"));
	
	return 0;
}