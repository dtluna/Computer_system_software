#include "functions.h"

struct pid_record add_process(struct pid_record pidrec)
{
	pid_t pid = 0;

	pid = fork();
	if (-1 == pid)
	{
		perror("fork");
		exit(EXIT_FAILURE);
	}

	if (0 == pid)
	{
		child_process();
	}
	else
	{
		pidrec.pid_array[pidrec.number_of_procs] = pid;
		pidrec.number_of_procs++;
		
		return pidrec;
	}
}

struct pid_record kill_last_process(struct pid_record pidrec)
{
	printf("Process #%d will be killed.",pidrec.pid_array[pidrec.number_of_procs-1]);
	if (-1 == kill(pidrec.pid_array[pidrec.number_of_procs-1], SIGTERM))
	{
		perror ("kill");
		_exit(EXIT_FAILURE);
	}
	else
	{
		puts("Success.");
	}
	pidrec.pid_array[pidrec.number_of_procs-1] = 0;
	pidrec.number_of_procs--;
	return pidrec;
}

void child_process()
{
	sigset_t child_set;
	int caught_signal;
	if(sigaddset(&child_set, SIGSEGV) == -1)
	{
		fprintf(stderr, "sigaddset error\n");
		perror("sigaddset");
	}

	int i = 0;
	char string[11] = "0123456789\0";
	
	while(1)
	{
		//printf("Child #%d: waiting.\n", getpid());
		//fflush(stdout);
		sigwait(&child_set, &caught_signal);
		//pause();
		//printf("Child #%d: caught signal = %d\n", getpid(), caught_signal);
		//fflush(stdout);
		i = 0;
		printf("Child #%d:", getpid());
		while(i < strlen(string))
		{
			//sleep(1);
			printf("%c", string[i]);
			i++;
			fflush(stdout);
		}
		puts("");
		//printf("Child #%d: sending SIGUSR1 to parent.\n", getpid());
		kill(getppid(), SIGSEGV);
	}
}

char* allocate_string (int length)
{
	char* string = (char*)calloc(length, sizeof(char));

	if (NULL == string)
	{
		fprintf(stderr, "allocate_string(): Memory allocation error.\n");
		kill(0, SIGTERM);
		exit(EXIT_FAILURE);
	}
	return string;
}
