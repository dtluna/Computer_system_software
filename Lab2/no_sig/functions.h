#include <sys/wait.h>
#include "pid_record.h"
#include <stdlib.h>
#include <unistd.h>
#include <linux/signal.h>
#include <string.h>
#include "getch.h"

struct pid_record add_process(struct pid_record pidrec);
void child_process();
struct pid_record kill_last_process(struct pid_record pidrec);

char* allocate_string (int length);
