#include "pid_record.h"

struct pid_record initialize_pid_record (struct pid_record pidrec)
{
	pidrec.number_of_procs = 0;
	for (int i = 0; i < PID_ARRAY_SIZE; ++i)
	{
		pidrec.pid_array[i] = -1;
	}
	return pidrec;
}