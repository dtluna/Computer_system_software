#include "functions.h"

/*Начальный процесс является управляющим.
Он принимает поток ввода с клавиатуры и контролирует дочерние процессы.
По нажатию клавиши ‘+’ добавляется новый процесс, 
‘-’ – удаляется последний добавленный, 
‘q’ – программа завершается. 
Каждый дочерний процесс посимвольно выводит на экран в вечном цикле свою уникальную строку. 
При этом операция вывода строки должна быть атомарной, 
т.е. процесс вывода должен быть синхронизирован таким образом, 
чтобы строки на экране не перемешивались. 
Выполняется в двух вариантах: под Linux и Windows. 
В качества метода синхронизации использовать сигналы/события.
*/

int main()
{	
	char c = 0;

	struct pid_record pidrec;
	pidrec = initialize_pid_record(pidrec);

	printf("Parent PID: #%d.\n", getpid());
	puts("'a' - adding a process\n'-' - killing a process\n'q' - exiting the program");

	while(1)
	{
		c = getch();

		if('a' == c)
		{
			pidrec = add_process(pidrec);
			printf("Created a new child process. PID: #%d.\n", pidrec.pid_array[pidrec.number_of_procs-1]);
			//printf("%d\n", pidrec.number_of_procs);
		}

		if ('q' == c)
		{
			while(pidrec.number_of_procs > 0)
			{
				pidrec = kill_last_process(pidrec);
			}
			//puts("Parent: — I will commit sudoku!");
			return 0;
		}

		if ('-' == c)
		{
			if (pidrec.number_of_procs > 0)
			{
				pidrec = kill_last_process(pidrec);
			}
			else
			{
				fprintf(stderr, "No child processes.\n");
			}
		}
		
		if(pidrec.number_of_procs>0)
		{
			sigset_t set;
			int caught_signal;
			sigaddset(&set, SIGSEGV);
				
			for (int i = 0; i < pidrec.number_of_procs; i++)
			{
				//sleep(1);
				//printf("Parent:[%d] sending SIGSEGV to child #%d.",i, pidrec.pid_array[i]);
				fflush(stdout);
				if (-1 == kill(pidrec.pid_array[i], SIGSEGV))
				{
					perror ("kill");
					_exit(EXIT_FAILURE);
				}
				else
				{
				//	puts("Success.");
				}
				sigwait(&set, &caught_signal);
				//puts("Parent: waiting for SIGSEGV.");
				
				//printf("Parent: caught singal #%d.\n", caught_signal);
			}
		}
	}
}
