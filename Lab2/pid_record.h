#include <sys/types.h>
#ifndef PID_ARRAY_SIZE
#define PID_ARRAY_SIZE 256
#endif
struct pid_record
{
	pid_t pid_array[PID_ARRAY_SIZE];
	unsigned short number_of_procs;
};

struct pid_record initialize_pid_record (struct pid_record pidrec);