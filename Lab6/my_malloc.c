#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_malloc.h"

#define handle_error(msg) \
	do { perror(msg); exit(EXIT_FAILURE); } while (0)

void* my_malloc (size_t nbytes)
{	
	nbytes = nbytes + sizeof(size_t);
	void* ptr = mmap (NULL, nbytes, 
		PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, NULL, NULL);
	if (MAP_FAILED == ptr)
		handle_error("mmap");
	size_t* nbytes_ptr = (size_t*)ptr;
	*nbytes_ptr = nbytes;
	ptr = ptr + sizeof(size_t);
	//puts("my_malloc");
	//printf("nbytes_ptr=%p\nptr=%p\n", nbytes_ptr, ptr);
	//printf("nbytes=%ld\n",nbytes );
	return ptr;
}

void* my_realloc (void* ptr, size_t new_nbytes)
{
	//puts("my_realloc");
	void* new_ptr = my_malloc(new_nbytes);

	char* oldptr = (char*)ptr;
	ptr = ptr - sizeof(size_t);
	size_t* old_nbytes_ptr = (size_t*)ptr;
	size_t old_nbytes = *old_nbytes_ptr;
	
	//printf("\nnew_ptr=%p\noldptr=%p\n", new_ptr, oldptr);
	//printf("old_nbytes=%ld\n",old_nbytes );

	char* new_ptr_copy = (char*)new_ptr;
	for (size_t i = 0; i < old_nbytes; ++i)
		new_ptr_copy[i] = oldptr[i];
	
	my_free(oldptr);
	return new_ptr;
}

void my_free (void* ptr)
{
	//puts("\nmy_free");
	//printf("ptr=%p\n", ptr);
	ptr = ptr - sizeof(size_t);
	size_t* nbytes_ptr = (size_t*)ptr;
	//printf("nbytes_ptr=%p\n", nbytes_ptr);
	size_t nbytes = *nbytes_ptr;
	//printf("nbytes=%ld\n",nbytes );

	if (-1 == munmap(ptr, nbytes))
		handle_error("munmap");
}
