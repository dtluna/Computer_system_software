#include <stdio.h>
#include <stdlib.h>
#include "my_malloc.h"
#include "getch.h"
#include <unistd.h>
#include <sys/mman.h>

#ifndef MiB
#define MiB(number) 1024*1024*number
#endif

int main(int argc, const char** argv)
{
	if (argc<2)
	{
		fprintf(stderr, "Usage:%s <amount of ints to allocate>.\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	size_t i = 0;
	size_t nmemb = atoll(argv[1]);

	int* array = (int*)my_malloc(nmemb*sizeof(int));
	for (i = 0; i < nmemb; ++i)
		array[i] = i+1;
	for (i = 0; i < nmemb; ++i)
		printf("a[%ld] = %d\n", i, array[i]);

	i = nmemb;
	
	nmemb=2*atoll(argv[1]);
	//printf("\nOld array adress = %p\n", array);
	//printf("Called realloc, nbytes=%ld\n", nmemb*sizeof(int));
	puts("New array is two times bigger than the original one.");
	array = (int*)my_realloc(array, nmemb*sizeof(int));
	//printf("New array adress = %p\n", array);
	for (; i < nmemb; ++i)
		array[i] = i+1;

	for (i = 0; i < nmemb; ++i)
		printf("a[%ld] = %d\n", i, array[i]);

	my_free(array);
	return 0;
}