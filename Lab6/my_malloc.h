#include <sys/types.h>

void* my_malloc (size_t nbytes);
void* my_realloc (void* ptr, size_t new_nbytes);
void my_free (void* ptr);
